package linkedlists;

/**
 * @author Akshay Reddy Vontari
 */
public class Tester {

    //this is a main method

    /**
     *
     * @param args command line args
     */
    public static void main(String[] args) {
        Vontari_ALinkedList<Integer> intList = new Vontari_ALinkedList<>();

        intList.addFirst(17);
        intList.addFirst(25);
        intList.addFirst(47);
        intList.removeFirst();
        intList.addFirst(55);

        System.out.println("Contents of linked list\n"
                + (intList.isEmpty() ? "list is empty" : intList));

        while (!intList.isEmpty()) {
            System.out.println("Deleting " + intList.removeFirst());
        }
        System.out.println();
        System.out.println("Contents of linked list\n"
                + (intList.isEmpty() ? "list is empty" : intList));
    }
}
