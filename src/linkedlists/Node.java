package linkedlists;


/**
 * @author Akshay Reddy Vontari
 * @param <E>
 */
public class Node<E>
{
   E data;
   Node<E> nextNode;

    /**constructor with two args
     *param data in node
     * param nextNode refers the nextNode from node
     * @param data in node
     * @param nextNode refers the nextNode from node
     */
    public Node(E data, Node<E> nextNode) {
        this.data = data;
        this.nextNode = nextNode;
    }
   
  
   /*
    Returns a string representation of the Node.
    @return a string representation of the Node.
    */
   @Override
   public String toString()
   {
      return data.toString();
   }
}
