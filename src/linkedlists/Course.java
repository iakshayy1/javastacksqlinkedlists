package linkedlists;

/**
 * @author Akshay Reddy Vontari
 */
public class Course {

    private String courseID;
    private String courseName;
    private int creditHours;

    /**
     *
     * @param courseID the id of course 
     * @param courseName the name of course
     * @param creditHours the number of creditHours
     */
    public Course(String courseID, String courseName, int creditHours) {
        this.courseID = courseID;
        this.courseName = courseName;
        this.creditHours = creditHours;
    }
    /*
    Returns a string representation of the course.
    @return a string representation of the course.
    */

    @Override
    public String toString() {
        return courseID + " " + creditHours + " " + courseName;
    }
}
