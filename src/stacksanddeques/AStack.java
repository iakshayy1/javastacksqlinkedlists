/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stacksanddeques;

import java.util.ArrayDeque;
import java.util.EmptyStackException;

/**
 *
 * @author Akshay Reddy Vontari
 * @param <E> type of AStack generic class
 */
public class AStack<E> {
    private ArrayDeque<E> myStack;

    /**
     *no args constructor of AStack which initialize myStack to ArrayDeque.
     */
    public AStack() {
        this.myStack=new ArrayDeque<E>();
    }

    /**
     *return the myStack elements
     * @return the myStack elements
     */
    public ArrayDeque<E> getMyStack() {
        return myStack;
    }

    /**
     *param myStack sets the myStack Deque
     * @param myStack set the myStack Deque.
     */
    public void setMyStack(ArrayDeque<E> myStack) {
        this.myStack = myStack;
    }
        
    /**
     *param element which to be push into the myStack
     * @param element which to be push into the myStack
     */
    public void push(E element) {
        myStack.push(element);
    }

    /**
     *return the value of myStack which is removed from front of stack
     * @return the value of myStack which is removed from front of stack
     */
    public E pop(){
       if(isEmpty()){
           throw new EmptyStackException();
       }
       else{
           return myStack.pop();
       }
//    return myStack.pop();
    } 
    
    /**
     *return the top element in the stack
     * @return the top element in the stack
     */
    public E peek(){
        return myStack.peek();
    } 
    
    /**
     *return the size of the stack
     * @return the size of the stack
     */
    public int size(){
        return myStack.size();
    }

    /**
     *return whether the stack is empty or not.
     * @return the true when stack is empty otherwise false.
     */
    public boolean isEmpty(){
      return  myStack.isEmpty();
    } 
    
    
    
}
